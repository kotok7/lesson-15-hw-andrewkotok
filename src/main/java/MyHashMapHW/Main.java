package MyHashMapHW;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<Integer, String> weekMap = new MyHashMap<>();
        Map<Integer, String> weekEnd = new MyHashMap<>();
        weekMap.put(1, "Mon");
        weekMap.put(2, "Tue");
        weekMap.put(3, "Wed");
        weekMap.put(4, "Thur");
        weekMap.put(5, "Fri");
        weekEnd.put(6, "Sat");
        weekEnd.put(7, "Sun");

        Set<Map.Entry<Integer, String>> entries = weekMap.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            System.out.printf("%d . %s\n", entry.getKey(), entry.getValue());
        }
        System.out.println("\n");
        System.out.println(weekMap.containsValue("Fri"));
        System.out.println("\n");
        weekMap.putAll(weekEnd);
        Set<Map.Entry<Integer, String>> entries2 = weekMap.entrySet();
        for (Map.Entry<Integer, String> entry : entries2) {
            System.out.printf("%d . %s\n", entry.getKey(), entry.getValue());
        }
        System.out.println("\n");

        weekMap.remove(2);
        Set<Map.Entry<Integer, String>> entries3 = weekMap.entrySet();
        for (Map.Entry<Integer, String> entry : entries3) {
            System.out.printf("%d . %s\n", entry.getKey(), entry.getValue());
        }
        System.out.println("\n");

        Collection<String> values = weekMap.values();
        for (String s : values) {
            System.out.printf("%s; ", s);
        }
        System.out.println("\n");


        weekMap.clear();
        Set<Map.Entry<Integer, String>> entries1 = weekMap.entrySet();
        for (Map.Entry<Integer, String> entry : entries1) {
            System.out.printf("%d . %s\n", entry.getKey(), entry.getValue());
        }
        System.out.println("clear() is working.");
    }
}
