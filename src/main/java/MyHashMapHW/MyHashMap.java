package MyHashMapHW;

import java.util.*;

public class MyHashMap<K, V> implements Map<K, V> {

    private static final int DEFAULT_CAPACITY = 16;

    private static class Node<K, V> implements Map.Entry<K, V> {
        final K key;
        V value;
        Node<K, V> next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        boolean hasValue(Object value) {
            return this.value.equals(value);
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            return this.value = value;
        }

        public boolean hasKey(Object key) {
            return this.key.equals(key);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?, ?> node = (Node<?, ?>) o;
            return Objects.equals(key, node.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }

        public Node<K, V> getNext() {
            return next;
        }

        public void setNext(Node<K, V> next) {
            this.next = next;
        }
    }

    private Node<K, V>[] nodes;
    private int size = 0;
    private int capacity;

    public MyHashMap(int capacity) {
        this.capacity = capacity;
        this.nodes = new Node[capacity];
    }

    public MyHashMap() {
        this(DEFAULT_CAPACITY);
    }

    private ArrayList<LinkedList<Node<K, V>>> list;


    @Override
    public V remove(Object key) {
        if (key == null) {
            throw new IllegalArgumentException();
        }
        int index = Math.abs(key.hashCode() % nodes.length);
        if (nodes[index] != null) {
            Node<K, V> prev = null;
            Node<K, V> node = nodes[index];
            while (node.next != null && !node.getKey().equals(key)) {
                prev = node;
                node = node.next;
            }
            if (node.getKey().equals(key)) {
                V value = node.getValue();
                if (prev == null) {
                    nodes[index] = node.getNext();
                } else {
                    prev.setNext(node.getNext());
                }
                size--;
                return value;
            }
        }
        throw new NoSuchElementException();
    }
    //remove(Object key) done according to the comments

    @Override
    public boolean containsValue(Object value) {
        for (Node<K, V> node : nodes)
            if (node != null && node.getValue().equals(value))
                return true;
        return false;
    }
//containsValue(Object value) done according to the comments


    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
//    putAll(Map<? extends K, ? extends V> m) done

    @Override
    public void clear() {
        size = 0;
        nodes = new Node[capacity];
    }
    //    clear() done

    @Override
    public Collection<V> values() {
        List<V> values = new ArrayList<>();
        for (Node<K, V> tmp : nodes) {
            if (tmp != null) {
                values.add(tmp.getValue());
            }
        }
        return values;
    }

//    values() done

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key == null) return false;
        int index = getIndex(key);
        for (Node cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasKey(key)) {
                return true;
            }
        }
        return false;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode() % nodes.length);
    }

    @Override
    public V get(Object key) {
        if (key == null) throw new UnsupportedOperationException();
        int index = getIndex(key);
        for (Node<K, V> cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasKey(key)) {
                return cur.getValue();
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        if (key == null) throw new UnsupportedOperationException();
        int index = getIndex(key);
        Node<K, V> node = new Node<>(key, value);
        if (nodes[index] == null) {
            nodes[index] = node;
            size++;
            return value;
        }
        for (Node<K, V> cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasKey(key)) {
                return cur.setValue(value);
            } else if (cur.next == null) {
                cur.next = node;
                size++;
                break;
            }
        }
        return value;
    }

    @Override
    public Set<K> keySet() {
        Set<K> result = new MyHashSet<>(capacity);
        for (int i = 0; i < capacity; i++) {
            for (Node<K, V> cur = nodes[i]; cur != null; cur = cur.next) {
                result.add(cur.getKey());
            }
        }
        return result;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> result = new MyHashSet<>(capacity);
        for (int i = 0; i < capacity; i++) {
            for (Node<K, V> cur = nodes[i]; cur != null; cur = cur.next) {
                result.add(cur);
            }
        }
        return result;
    }

}
